﻿using Microsoft.AspNetCore.Mvc;
using webMVC.Models;

namespace webMVC.Controllers
{
    public class UsersController : Controller
    {
        private static List<UsersViewModel> _usersViewModels = new List<UsersViewModel>()
        {
            new UsersViewModel(1, "Irvan", 22947234, "Surabaya", "Backend Developer"),
            new UsersViewModel(2, "Reza", 27492058, "Jakarta", "Backend Developer"),
            new UsersViewModel(3, "Fatima", 93755634, "Palembang", "Frontend Developer"),
            new UsersViewModel(4, "Tertia", 98476234, "Bandung", "Frontend Developer"),
            new UsersViewModel(5, "Ariza", 76500474, "Medan", "QA Analyst"),
        };
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Add()
        {
            return View();
        }

        public IActionResult List()
        {
            return View(_usersViewModels);
        }

        [HttpPost]
        public IActionResult Save([Bind("Id, Nama, NIK, TempatLahir, JobPosition")] UsersViewModel users)
        {
            _usersViewModels.Add(users);
            return Redirect("List");
        }
        public IActionResult Edit(int? id)
        {
            UsersViewModel users = _usersViewModels.Find(x => x.Id.Equals(id));
            return View(users);
        }
        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, Nama, NIK, TempatLahir, JobPosition")] UsersViewModel users)
        {
            UsersViewModel usersOld = _usersViewModels.Find(x => x.Id.Equals(id));
            _usersViewModels.Remove(usersOld);

            _usersViewModels.Add(users);
            return Redirect("List");
        }
        public IActionResult Details(int id)
        {
            UsersViewModel users= (
                from u in _usersViewModels
                where u.Id.Equals(id)
                select u
                ).SingleOrDefault(new UsersViewModel());
            return View(users);
        }
        public IActionResult Delete(int? id)
        {
            UsersViewModel users = _usersViewModels.Find(x => x.Id.Equals(id));
            _usersViewModels.Remove(users);

            return View(users);
        }
    }
}
