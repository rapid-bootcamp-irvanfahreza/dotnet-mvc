﻿using Microsoft.AspNetCore.Mvc;
using webMVC.Models;

namespace webMVC.Controllers
{
    public class AbsentController : Controller
    {
        private static List<AbsentViewModel> _absentViewModels = new List<AbsentViewModel>()
        {
            new AbsentViewModel(1, 1001, "Irvan", "Laki-laki", "CEO"),
            new AbsentViewModel(2, 1002, "Reza", "Laki-laki", "CFO"),
            new AbsentViewModel(3, 1003, "Fatima", "Perempuan", "CTO"),
            new AbsentViewModel(4, 1004, "Ariza", "Perempuan", "COO"),
        };

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Add() 
        { 
            return View();
        }

        public IActionResult List()
        {
            return View(_absentViewModels);
        }

        [HttpPost]
        public IActionResult Save([Bind("Id, NIK, Nama, JenisKelamin, Jabatan")] AbsentViewModel absent)
        {
            _absentViewModels.Add(absent);
            return Redirect("List");
        }

        public IActionResult Edit(int? id)
        {
            AbsentViewModel absent = _absentViewModels.Find(x => x.Id.Equals(id));
            return View(absent);
        }

        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, NIK, Nama, JenisKelamin, Jabatan")] AbsentViewModel absent)
        {
            AbsentViewModel absentOld = _absentViewModels.Find(x => x.Id.Equals(id));
            _absentViewModels.Remove(absentOld);

            _absentViewModels.Add(absent);
            return Redirect("List");
        }

        public IActionResult Details(int id)
        {
            AbsentViewModel absent = (
                from a in _absentViewModels
                where a.Id.Equals(id)
                select a
                ).SingleOrDefault(new AbsentViewModel());
            return View(absent);
        }

        public IActionResult Delete(int? id)
        {
            AbsentViewModel absent = _absentViewModels.Find(x => x.Id.Equals(id));
            _absentViewModels.Remove(absent);

            return View(absent);
        }
    }
}
