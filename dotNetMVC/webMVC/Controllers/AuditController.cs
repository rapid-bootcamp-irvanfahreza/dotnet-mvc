﻿using Microsoft.AspNetCore.Mvc;
using webMVC.Models;

namespace webMVC.Controllers
{
    public class AuditController : Controller
    {
        private static List<AuditViewModel> _auditViewModels = new List<AuditViewModel>()
        {
            new AuditViewModel(1, 2001, "Acer 9", "i7", "K264GS", "Irvan", "Windows 11", "Office 2019"),
            new AuditViewModel(2, 2002, "HP 7", "i3", "J47SG4", "Fahreza", "Windows 8", "Office 2017"),
            new AuditViewModel(3, 2003, "Acer 25", "i5", "SHG85H", "Fatima", "Windows 11", "Office 2017"),
            new AuditViewModel(4, 2004, "ROG 6", "i9", "SGF782", "Tertia", "Windows 10", "Office 2021"),
            new AuditViewModel(5, 2005, "Dell 2", "i9", "HSGG92", "Ariza", "Windows 11", "Office 2019"),
        };

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Add()
        {
            return View();
        }

        public IActionResult List()
        {
            return View(_auditViewModels);
        }

        [HttpPost]
        public IActionResult Save([Bind("Id, AssetNo, AssetName, AssetType, SerialNo, PIC, TypeWindows, TypeOffice")] AuditViewModel audit)
        {
            _auditViewModels.Add(audit);
            return Redirect("List");
        }

        public IActionResult Edit(int? id)
        {
            AuditViewModel audit = _auditViewModels.Find(x => x.Id.Equals(id));
            return View(audit);
        }

        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, AssetNo, AssetName, AssetType, SerialNo, PIC, TypeWindows, TypeOffice")] AuditViewModel audit)
        {
            AuditViewModel auditOld = _auditViewModels.Find(x => x.Id.Equals(id));
            _auditViewModels.Remove(auditOld);

            _auditViewModels.Add(audit);
            return Redirect("List");
        }

        public IActionResult Details(int id)
        {
            AuditViewModel audit = (
                from a in _auditViewModels
                where a.Id.Equals(id)
                select a
                ).SingleOrDefault(new AuditViewModel());
            return View(audit);
        }

        public IActionResult Delete(int? id)
        {
            AuditViewModel audit = _auditViewModels.Find(x => x.Id.Equals(id));
            _auditViewModels.Remove(audit);

            return View(audit);
        }
    }
}
