﻿using Microsoft.AspNetCore.Mvc;
using webMVC.Models;

namespace webMVC.Controllers
{
    public class ProductController : Controller
    {

        private static List<ProductViewModel> _productViewModels = new List<ProductViewModel>()
        {
            new ProductViewModel(1, "Mie Ayam", "Makanan", 19000),
            new ProductViewModel(2, "Mie Ayam Bakso", "Makanan", 22000),
            new ProductViewModel(3, "Bakso Granat", "Makanan", 17000),
            new ProductViewModel(4, "Es Jeruk", "Minuman", 4000),
            new ProductViewModel(5, "Es Teh", "Minuman", 3000),
        };

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Add()
        {
            return View();
        }

        public IActionResult List()
        {
            return View(_productViewModels);
        }

        [HttpPost]
        public IActionResult Save([Bind("Id, Name, Category, Price")] ProductViewModel product)
        {
            _productViewModels.Add(product);
            return Redirect("List");
        }

        public IActionResult Edit(int? id)
        {
            // cari dengan lambda
            ProductViewModel product = _productViewModels.Find(x => x.Id.Equals(id));
            return View(product);
        }

        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, Name, Category, Price")] ProductViewModel product)
        {
            // hapus data yg lama
            ProductViewModel productOld = _productViewModels.Find(x => x.Id.Equals(id));
            _productViewModels.Remove(productOld);

            //input data yg baru
            _productViewModels.Add(product);
            return Redirect("List");
        }

        public IActionResult Details(int id)
        {
            //cari dengan LINQ
            ProductViewModel product = (
                from p in _productViewModels
                where p.Id.Equals(id)
                select p
                ).SingleOrDefault(new ProductViewModel());
            return View(product);
        }

        public IActionResult Delete(int? id)
        {
            //cari data
            ProductViewModel product = _productViewModels.Find(x => x.Id.Equals(id));
            //hapus dari list
            _productViewModels.Remove(product);

            return View(product);
        }
    }
}
