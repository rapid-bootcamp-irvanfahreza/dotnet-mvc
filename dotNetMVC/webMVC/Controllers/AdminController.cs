﻿using Microsoft.AspNetCore.Mvc;
using webMVC.Models;

namespace webMVC.Controllers
{
    public class AdminController : Controller
    {
        private static List<AdminViewModel> _adminViewModels = new List<AdminViewModel>()
        {
            new AdminViewModel(1, "Admin Irvan", 2856234, "Laki-laki", "Admin HR"),
            new AdminViewModel(2, "Admin Reza", 2752875, "Laki-laki", "Admin HR"),
            new AdminViewModel(3, "Admin Fatima", 2554836, "Perempuan", "Admin HR"),
            new AdminViewModel(4, "Admin Tertia", 2576534, "Perempuan", "Admin HR"),
            new AdminViewModel(5, "Admin Arizaz", 2097124, "Perempuan", "Admin HR"),
        };
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Add()
        {
            return View();
        }
        public IActionResult List()
        {
            return View(_adminViewModels);
        }

        [HttpPost]
        public IActionResult Save([Bind("Id, Nama, NIK, JenisKelamin, Position")] AdminViewModel admin)
        {
            _adminViewModels.Add(admin);
            return Redirect("List");
        }

        public IActionResult Edit(int? id)
        {
            AdminViewModel admin = _adminViewModels.Find(x => x.Id.Equals(id));
            return View(admin);
        }

        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, Nama, NIK, JenisKelamin, Position")] AdminViewModel admin)
        {
            AdminViewModel adminOld = _adminViewModels.Find(x => x.Id.Equals(id));
            _adminViewModels.Remove(adminOld);

            _adminViewModels.Add(admin);
            return Redirect("List");
        }

        public IActionResult Details(int id)
        {
            AdminViewModel admin = (
                from a in _adminViewModels
                where a.Id.Equals(id)
                select a
                ).SingleOrDefault(new AdminViewModel());
            return View(admin);
        }

        public IActionResult Delete(int? id)
        {
            AdminViewModel admin = _adminViewModels.Find(x => x.Id.Equals(id));
            _adminViewModels.Remove(admin);

            return View(admin);
        }
    }
}
