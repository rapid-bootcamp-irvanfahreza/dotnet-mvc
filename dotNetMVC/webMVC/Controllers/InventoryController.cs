﻿using Microsoft.AspNetCore.Mvc;
using webMVC.Models;

namespace webMVC.Controllers
{
    public class InventoryController : Controller
    {
        private static List<InventoryViewModel> _inventoryViewModels = new List<InventoryViewModel>()
        {
            new InventoryViewModel(1, 2002, "Asus 6", "i3", "K2J47F", 2019),
            new InventoryViewModel(2, 2003, "Acer 13", "i7", "P382GF", 2018),
            new InventoryViewModel(3, 2004, "Dell 9", "i5", "H3J28F", 2021),
            new InventoryViewModel(4, 2005, "Lenovo", "i9", "MX8BB3", 2020),
            new InventoryViewModel(5, 2006, "HP", "i3", "L2K38G", 2019),
        };

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Add()
        {
            return View();
        }

        public IActionResult List()
        {
            return View(_inventoryViewModels);
        }


        [HttpPost]
        public IActionResult Save([Bind("Id, NoAsset, AssetName, AssetType, SerialNo, PurchaseYear")] InventoryViewModel inventory)
        {
            _inventoryViewModels.Add(inventory);
            return Redirect("List");
        }

        public IActionResult Edit(int? id)
        {
            InventoryViewModel inventory = _inventoryViewModels.Find(x => x.Id.Equals(id));
            return View(inventory);
        }

        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, NoAsset, AssetName, AssetType, SerialNo, PurchaseYear")] InventoryViewModel inventory)
        {
            InventoryViewModel inventoryOld = _inventoryViewModels.Find(x => x.Id.Equals(id));
            _inventoryViewModels.Remove(inventoryOld);

            _inventoryViewModels.Add(inventory);
            return Redirect("List");
        }

        public IActionResult Details(int id)
        {
            InventoryViewModel inventory = (
                from i in _inventoryViewModels
                where i.Id.Equals(id)
                select i
                ).SingleOrDefault(new InventoryViewModel());
            return View(inventory);
        }

        public IActionResult Delete(int? id)
        {
            InventoryViewModel inventory = _inventoryViewModels.Find(x => x.Id.Equals(id));
            _inventoryViewModels.Remove(inventory);

            return View(inventory);
        }
    }
}
