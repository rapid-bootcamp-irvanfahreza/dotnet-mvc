﻿using Microsoft.AspNetCore.Mvc;

namespace webMVC.Models
{
    public class UsersViewModel
    {
        public int Id { get; set; }
        public string Nama { get; set; }
        public int NIK { get; set; }
        public string TempatLahir { get; set; }
        public string JobPosition { get; set; }

        public UsersViewModel()
        {

        }

        public UsersViewModel(int id, string nama, int nik, string tempatLahir, string jobPosition)
        {
            Id = id;
            Nama = nama;
            NIK = nik;
            TempatLahir = tempatLahir;
            JobPosition = jobPosition;
        }
    }
}
