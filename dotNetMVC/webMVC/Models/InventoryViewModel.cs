﻿namespace webMVC.Models
{
    public class InventoryViewModel
    {
        public int Id { get; set; }
        public int NoAsset { get; set; }
        public string AssetName { get; set; }
        public string AssetType { get; set; }
        public string SerialNo { get; set; }
        public int PurchaseYear { get; set; }

        public InventoryViewModel() 
        {

        }

        public InventoryViewModel(int id, int noAsset, string assetName, string assetType, string serialNo, int purchaseYear)
        {
            Id = id;
            NoAsset = noAsset;
            AssetName = assetName;
            AssetType = assetType;
            SerialNo = serialNo;
            PurchaseYear = purchaseYear;
        }
    }
}
