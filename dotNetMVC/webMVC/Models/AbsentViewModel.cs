﻿namespace webMVC.Models
{
    public class AbsentViewModel
    {
        public int Id { get; set; }
        public int NIK { get; set; }
        public string Nama { get; set; }
        public string JenisKelamin { get; set; }
        public string Jabatan { get; set; }

        public AbsentViewModel()
        {
        }

        public AbsentViewModel(int id, int nik, string nama, string jeniskelamin, string jabatan)
        {
            Id = id;
            NIK = nik;
            Nama = nama;
            JenisKelamin= jeniskelamin;
            Jabatan = jabatan;
        }
    }
}
