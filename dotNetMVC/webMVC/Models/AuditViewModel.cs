﻿using Microsoft.AspNetCore.Mvc;

namespace webMVC.Models
{
    public class AuditViewModel
    {
        public int Id { get; set; }
        public int AssetNo { get; set; }
        public string AssetName { get; set; }
        public string AssetType { get; set; }
        public string SerialNo { get; set; }
        public string PIC { get; set; }
        public string TypeWindows { get; set; }
        public string TypeOffice { get; set; }

        public AuditViewModel()
        {

        }

        public AuditViewModel(int id, int assetNo, string assetName, string assetType, string serialNo, string pic, string typeWindows, string typeOffice)
        {
            Id = id;
            AssetNo = assetNo;
            AssetName = assetName;
            AssetType = assetType;
            SerialNo = serialNo;
            PIC = pic;
            TypeWindows = typeWindows;
            TypeOffice = typeOffice;
        }
    }
}
