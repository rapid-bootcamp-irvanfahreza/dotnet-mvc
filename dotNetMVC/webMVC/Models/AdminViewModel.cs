﻿using Microsoft.AspNetCore.Mvc;

namespace webMVC.Models
{
    public class AdminViewModel
    {
        public int Id { get; set; }
        public string Nama { get; set; }
        public int NIK { get; set; }
        public string JenisKelamin { get; set; }
        public string Position { get; set; }

        public AdminViewModel()
        {

        }

        public AdminViewModel(int id, string nama, int nik, string jenisKelamin, string position)
        {
            Id = id;
            Nama = nama;
            NIK = nik;
            JenisKelamin = jenisKelamin;
            Position = position;
        }
    }
}
